package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    List<FridgeItem> storage = new ArrayList<>();
    @Override
    public int nItemsInFridge() {
        return storage.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(storage.size()<totalSize()){
            storage.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!storage.contains(item)){
            throw new NoSuchElementException();
        }
        storage.remove(item);
    }

    @Override
    public void emptyFridge() {
        storage = new ArrayList<>();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> ExpierdFood = new ArrayList<>();
        for (FridgeItem e:storage) {
            if (e.hasExpired()){
                ExpierdFood.add(e);
            }
        }
        for (FridgeItem i:ExpierdFood) {
            storage.remove(i);
        }
        return ExpierdFood;
    }
}
